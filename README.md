# gemtext

"The simplest gemini server that could possibly work." ™

Written in C++, this repo is designed to get you up-and-running as simply as possible. It contains  gemini server and example site. Clone the repo and replace everything in the `doc` dir with your own content.

## Debian (Bookworm) prerequisites

```
sudo apt install libssl-dev libssl-doc
sudo apt install libcurl4-openssl-dev libcurl4-doc
```


## Server setup

```
cd gemini
./mkcerts # created public and private certificates in ~/.gemini
make
./gemd # start server
```

To have the server run at boot, you can use a variety of methods. 
E.g. have something like the following in `/etc/rc.local`:
```
#!/bin/sh
sudo -u pi /data/data/repos/gemtext/gemini/gemd & # tailor accordingly
```

## References

* [httpserver](https://github.com/devimalka/httpserver) - written in C
