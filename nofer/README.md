# nofer - a gopher server of gmi files

## Installation

### Void Linux

sudo xi openssl-devel libcurl-devel


## Status

2025-02-11  Tabs are replaced by U+0089 ("character tabulation with 
justification" because tabs have a special meaning in mapfiles: they 
separate out the fields.
