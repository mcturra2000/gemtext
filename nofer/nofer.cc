#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <sys/socket.h>
//#include <sys/types.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <curl/curl.h>
#include <setjmp.h>
#include <regex.h>

#include <algorithm>
#include <chrono>
#include <exception>
#include <filesystem>
#include <functional>
#include <iostream>
#include <fstream>
//#include <format> // still not in g++ 12.2.0

using namespace std;
namespace fs = std::filesystem;


string fqdn{"beelink.local"}; // set properly in main()
const int port = 70;

void puke(string msg)
{
	throw std::runtime_error(msg);
}

fs::path docdir; // path to where the gmi files are

// determine if fullname lies outside base - a cracking attempt
bool bad_filename_attempt(fs::path& base, fs::path& full)
{
	string basestr{base};
	string fullstr{full};
	if(fullstr.size() < basestr.size()) return true;
	return basestr != fullstr.substr(0, basestr.size());
}

class Client {
	public:
		Client(int sock) {
			struct sockaddr_in addr;
			unsigned int len = sizeof(addr);
			client = accept(sock, (struct sockaddr*)&addr, &len);
			if (client < 0) {
				puke("Client ctor: Unable to accept");
			}
		};
		~Client() {
			if(client>=0) 
				close(client);
			else
				cout << "That's odd. client<0" << endl;
		};
		const int operator()() { return client; };
		ssize_t write(const string& msg);
		ssize_t writeln(const string& msg);
	private:
		int client = -1;
};

ssize_t Client::writeln(const string& msg)
{
	return write(msg + "\r\n");
}

ssize_t Client::write(const string& msg)
{
	ssize_t nwritten = 0;
	ssize_t unwritten = msg.size();
	while(unwritten >0) {
		ssize_t written = ::write(client, msg.c_str(), unwritten);
		nwritten += written;
		unwritten -= written;
	}
	return nwritten;
}

int create_socket(int port, int maxNumConnections)
{
	int s;

	s = socket(AF_INET, SOCK_STREAM, 0);
	if (s < 0) {
		perror("Unable to create socket");
		exit(EXIT_FAILURE);
	}

	// allow for socket reuse so we don't get "Unable to bind: Address already in use"
	int yes=1; //char yes='1'; // use this under Solaris
	if (setsockopt(s, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) == -1) {
		perror("setsockopt");
		exit(1);
	}

	struct sockaddr_in addr;
	memset(&addr, 0, sizeof(struct sockaddr_in));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = htonl(INADDR_ANY);
	//addr.sin_addr.s_addr = inet_addr("192.168.0.13");
	//addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	if (bind(s, (struct sockaddr*)&addr, sizeof(addr)) < 0) {
		perror("Unable to bind");
		exit(EXIT_FAILURE);
	}

	if (listen(s, maxNumConnections) < 0) {
		perror("Unable to listen");
		exit(EXIT_FAILURE);
	}

	return s;
}
class Socket {
	public:
		Socket(int port, int maxNumConnections ) {
			sock = create_socket(port, maxNumConnections);
		};
		~Socket() {
			printf("Socket: closing\n");
			close(sock);
		};
		int& operator()() { return sock; };
	private:
		int sock;

};


jmp_buf ctl_c;

//bool stop =false;
void int_handler(int dummy) // Ctl-C
{
	printf("int_handler called\n");
	longjmp(ctl_c, 1);
}




string get_input(Client& client)
{
	string path{"/index.gmi"};
	char buf[200];
	int n = read(client(), buf, sizeof(buf));
	n = min(n, (int) sizeof(buf)-1);
	buf[n] = 0;
	for(int i = 0; buf[i]; i++) if(buf[i] == '\r' || buf[i] == '\n') buf[i] = 0;
	n = strlen(buf);
	if(n == 0) return path;
	if((n == 1) && (buf[0] == '/')) return path;
	path = buf;
	if(buf[0] != '/') path = '/' + path;
	return path;
}

std::string slurp(const char *filename)
{
	std::ifstream in;
	in.open(filename, std::ifstream::in | std::ifstream::binary);
	std::stringstream sstr;
	sstr << in.rdbuf();
	in.close();
	return sstr.str();
}

std::string slurp(const string& filename)
{
	return slurp(filename.c_str());
}

string extract_part (const string& url, CURLUPart what, unsigned int flags = CURLU_NON_SUPPORT_SCHEME)
{
	string retval;
	CURLU *h = curl_url();
	CURLUcode rc;
	rc = curl_url_set(h, what, url.c_str(), flags); // unsupported in the sense of 'gemini'
	if(rc) {
		//printf("curl_url_set returned error code %d\n", rc);
		return retval;
	}
	char *part;
	//strncpy(url_path, url.c_str(), sizeof(url_path)-1);
	string upath;
	rc = curl_url_get(h, what, &part, flags);
	if(rc) {
		//printf("curl_url_get returned error code %d\n", rc);
		return retval;
	} else {
		retval = part;
		curl_free(part);
	}
	curl_url_cleanup(h);
	return retval;
}


// Tabs have special meaning in Gopher, so we can't just embed them
// The client will need to figure the encoding out
string encodeTabs (const string& text)
{
	string res = "";
	for(auto c : text) {
		if(c == '\t')
			res += "\u0089"; // "character tabulation with justification"
		else
			res += c;
	}
	return res;
}

void print_link (Client& client, const string& url, const string& desc)
{
	string desc1{desc};
	desc1 = encodeTabs(desc1);

	auto say = [](const string& desc, const string& val) { 
		cout << desc << "=" << val << endl; 
		return; } ;

	string scheme, authority, rest{url};
	size_t found = url.find(":");
	if(found!= string::npos) {
		scheme = rest.substr(0, found);
		rest = rest.substr(found+1);
	}
	//say("rest", rest);

	found = rest.find("//");
	if(found!= string::npos) {
		authority = rest.substr(0, found);
		rest = rest.substr(found+1);
	}

	//say("scheme", scheme);
	//say("authority", authority);
	//say("rest", rest);

	auto starts = [&](const string& str) { return url.starts_with(str); };
	if(starts("gemini:") || starts("http:") || starts("https:") || starts("spartan:")) {
		string host{extract_part(url, CURLUPART_HOST)};
		client.writeln("h" + desc1 +"\tURL:" + url + "\t" + host + "\t70");
		return;
	}

	if(url.ends_with(".gmi")) {
		const string pstring = to_string(port);
		client.writeln("1" + desc1 + "\t/" + url + "\t" + fqdn + "\t" + pstring);
		return;
	}

	if(url.ends_with(".txt")) {
		const string pstring = to_string(port);
		client.writeln("0" + desc1 + "\t/" + url + "\t" + fqdn + "\t" + pstring);
		return;
	}

	if(starts("gopher://")) {
		string rest{url.substr(9)}; // chop out "gopher://" part
		string host{extract_part(url, CURLUPART_HOST, CURLU_PUNYCODE)};
	}


}

void handle_mapfile (Client& client, const string& fname)
{
	static regex_t link_re;
	if(regcomp(&link_re, "^=>\\s+(\\S+)\\s+(.*)", REG_EXTENDED)) { 
		cout << "regcomp didn't compile" << endl; 
		return;
	}
	size_t nmatch =3;
	regmatch_t pmatch[3];

	std::ifstream in;
	in.open(fname, std::ifstream::in | std::ifstream::binary);
	for(string line; getline(in, line);) {
		int status = regexec(&link_re, line.c_str(), nmatch, pmatch, 0);
		if(status == 0) {
			// link found
			auto text = [&](int n) { 
				string res;
				if(pmatch[n].rm_so == -1) return res;
				for(int i = pmatch[n].rm_so; i < pmatch[n].rm_eo; i++)
					res += line[i];
				return res;
			};
			print_link(client, text(1), text(2)); // print url, desc

		} else {
			line = encodeTabs(line);
			line = "i" + line + "\t\tnull.host\t1\r\n";
			client.write(line);
		}
	}
	in.close();
	client.writeln("."); // terminator char for mapfile
}


void handle_txtfile (Client& client, const string& fname)
{
	const string txt = slurp(fname);
	client.write(txt);
}

void handle_nonmapfile (Client& client, const string& fname)
{
	client.writeln("TODO");
}

void handle_connection(Client& client)
{
	string selector{get_input(client)};
	auto fname = docdir;
	fname.concat(selector);
	error_code ec;
	fname = canonical(fname, ec);

	const auto now = chrono::system_clock::now();
	const time_t t_c = std::chrono::system_clock::to_time_t(now);
	string now_str = ctime(&t_c);
	now_str = now_str.substr(0, now_str.size() -1); // chop of newline
	static int connection_count = 0;
	cout << connection_count++ << "\t" << now_str << " " << fname << endl;
	if(ec || bad_filename_attempt(docdir, fname)) {
		client.writeln("3Error: File or directory not found!");
		return;
	}

	string fnamestr{fname};
	string ext;
	if(fnamestr.size() > 4)  ext = fnamestr.substr(fnamestr.size()-4);
	if(ext == ".gmi")
		handle_mapfile(client, fnamestr);
	else if(ext == ".txt")
		handle_txtfile(client, fnamestr);
	else
		handle_nonmapfile(client, fnamestr);

}

// https://stackoverflow.com/questions/504810/how-do-i-find-the-current-machines-full-hostname-in-c-hostname-and-domain-info
// 25/2 Doesn't work on Void Linux
string get_fqdn()
{
	string result;
	struct addrinfo hints, *info, *p;
	int gai_result;

	char hostname[1024];
	hostname[1023] = '\0';
	gethostname(hostname, 1023);

	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC; /*either IPV4 or IPV6*/
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_CANONNAME;

	if ((gai_result = getaddrinfo(hostname, "http", &hints, &info)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(gai_result));
		exit(1);
	}

	for(p = info; p != NULL; p = p->ai_next) {
		printf("hostname: %s\n", p->ai_canonname);
		result = p->ai_canonname;
		break; // just take the first one
	}

	freeaddrinfo(info);
	return result;
}

string get_fqdn_XXX()
{
	char buf[256];
	gethostname(buf, sizeof(buf));
	string result{buf};
	if(getdomainname(buf, sizeof(buf)) == -1) { // seems to return (none)
		printf("getdomainname err: %d\n", errno);
	}
	puts(buf);
	return "TODO";
}

int main(int argc, char **argv)
{
	// work out where the docs dir is
	auto fs1 = fs::path(argv[0]);
	auto fs2 = canonical(fs1);
	auto fs3 = fs2.remove_filename(); // /abs/path/to/exe/ (with trailing /)
	fs3 += "../gdocs";
	docdir = canonical(fs3);
	cout << docdir << endl;

	int status;

	signal(SIGPIPE, SIG_IGN); /* Ignore broken pipe signals */
	signal(SIGINT, int_handler); // handle Ctl-C

	//fqdn = get_fqdn();

	Socket socket(port, 1);
	if(setjmp(ctl_c) == 0) {
		/* Handle connections */
		while(1) {
			//std::exception_ptr eptr;
			try {
				Client client(socket());
				handle_connection(client);
				int status = shutdown(client(), SHUT_RDWR);
				if(status == -1) {
					cout << "shutdown failed with error " << errno << endl;
				}
			} catch (const std::exception& ex) {
				//eptr = std::current_exception();
				cout << "Exception caught: " << ex.what() << endl;
			}
		}
	}
	printf("main exiting\n");

	return 0;
}
