#pragma once

/*
 * 2023-06-08   Created
 * https://vocal.com/resources/development/how-do-i-make-a-tls-client-connection-using-openssl/
 */
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <openssl/ssl.h>
#include <errno.h>
#include <netdb.h>
#include <openssl/err.h>
#include <curl/curl.h>

#include <algorithm>
#include <cassert>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <ranges>
#include <regex>
#include <string_view>
#include <variant>
#include <vector>




using namespace std;

// FN link_t 
typedef struct {
	string url;
	string desc;
	string icon; // a natty little icon to show type
	int num = -1; // an index number starting from 0
} link_t;
// FN-END


template<typename...Func>
struct overload : Func... {
	using Func::operator()...;
};

template<typename...Func> overload(Func...) -> overload<Func...>;



typedef variant<link_t, string> phrase_t;
typedef vector<phrase_t> phrases_t;

typedef vector<string> strings;
typedef vector<link_t> links_t;

/*
// page out stream
class postream : public std::ostream {
        public:
                postream() {};
                ~postream() {};
                friend postream& operator<<(postream& pos, const string& str);
		friend postream& operator<<(postream& pos;, const char* str);



};
*/

//inline class postream pout;
extern std::ostream pout;


int		fetch (string& response, const string& url);
links_t		links_resolve(phrases_t& phrases, const string& url_in);
phrases_t	parseResponse(const string& response);
void 		print(const phrase_t& phrase);
void 		println(const string& str);
void 		test_urls();

