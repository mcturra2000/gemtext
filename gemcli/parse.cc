#include "main.h"


static const std::regex rl("^=>\\s*(\\S+)\\s+(.+)");
static std::smatch ml;


phrases_t  parseResponse(const string& response)
{
	phrases_t ps;
	enum { start };
	int state = start;
	for (const auto line : std::views::split(response, "\n"sv)) {
		string str{std::string_view(line)};
		if(std::regex_match(str, ml, rl)) {
			ps.push_back(link_t{ml[1], ml[2]});
		} else {
			ps.push_back(str);
		}
	}
	return ps;
}

