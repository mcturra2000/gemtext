#include "main.h"




class ncurses {
	public:
		ncurses();
		~ncurses();
};


ncurses::ncurses()
{
}
ncurses::~ncurses()
{
}

// FN oneshot 
links_t oneshot(string url)
{
	string response;
	if(fetch(response, url))
		cerr << "Fetch didn't succeed\n";

	//cout <<response << endl;

	println("=================");
	auto phrases = parseResponse(response);
	auto links = links_resolve(phrases, url);
	for(auto& phrase : phrases) {
		print(phrase);
	}
	return links;
}
// FN-END

std::ostream pout(nullptr);


// FN repl 
void repl(string url)
{
	strings trail; /* the trail of urls that we visit */
	trail.push_back(url);

	while(1) {
		std::filebuf fb;
		char tmpl[] = "/tmp/gemcli.out"; // CAVEAT files get overwritten
						 //bool ok = std::tmpnam(tmpl);
						 //assert(ok);
		fb.open(tmpl,  std::ios_base::out );
		pout.rdbuf( &fb);
		links_t links = oneshot(url);
		fb.close();
		system("less /tmp/gemcli.out");


		// TODO abstract out
		while(1) {
			cout << "?" ;
			string input;
			getline(cin, input);
			if(input == "q") return;
			if(input == "h") {
				/* go back */
				if(trail.size() == 0) {
					cerr << "Can't go back. Already at beginning\n";
					continue;
				}
				url = trail[trail.size() -1];
				trail.pop_back();
				break;
				//url = link.url;
			} else if(input == "s") {
				cout << "Trail is:\n";
				for(auto& t : trail) cout << t << endl;
				continue;
			} else	{ 
				/* follow a link */
				int link_num = std::stoi(input);
				url = links[link_num].url;
				break;
			}

		}		

		trail.push_back(url);

	}

}

int main (int argc, char *argv[])
{
	string url = "gemini://beelink.local";
	//url = "gemini://beelink.local/vscode.gmi";

	pout.rdbuf(std::cout.rdbuf());
	if constexpr(false) test_urls(); // never returns

	if(argc==2)  url = string{argv[1]};

	if constexpr(true)
		repl(url);
	else
		oneshot(url);

	return 0;

}
