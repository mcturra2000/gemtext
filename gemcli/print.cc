#include "main.h"

#if 0
// page out stream
class postream : public std::ostream {
	public:
		postream() {};
		~postream() {};
		friend postream& operator<<(postream& pos, const string& str);


};
postream& operator<<(postream& pos, const string& str)
{
	cout << str;
	return pos;
}

postream& operator<<(postream& pos, const char* str)
{
	string s{str};
	return pos << s;
}


postream& endl(postream& pos)
{
	//cout << endl;
	pos << "\n";
	return pos;
}

//class postream pout;

#endif

void println(const string& str)
{
	pout << str << endl;
}


void print_link(const link_t& link)
{
	pout << link.icon << "[" << link.num << "] "  
		<< link.desc << endl;
	pout << "\t… " << link.url << endl;
}

void print_text(const string& text)
{
	pout << text << endl;
}


void print(const phrase_t& phrase)
{
	overload ovld {
		[&](link_t link) { print_link(link); },
		[](string str) { print_text(str); }
	};

	std::visit(ovld, phrase);
}
