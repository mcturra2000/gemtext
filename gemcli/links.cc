#include "main.h"

/*
   userinfo       host      port
   ┌──┴───┐ ┌──────┴──────┐ ┌┴─┐
https://john.doe@www.example.com:1234/forum/questions/?tag=net&foor=newt#top
└─┬─┘   └─────────────┬─────────────┘└───────┬───────┘ └────────────┬──┘ └┬┘
scheme            authority                path                   query          fragment
*/

string get_url_part(const string& url, CURLUPart part)
{
	CURLU *h = curl_url();
	CURLUcode rc = curl_url_set(h, CURLUPART_URL, url.c_str(),
			CURLU_NON_SUPPORT_SCHEME);
	if(rc)  return "";

	char* ptr;
	string result;
	rc = curl_url_get(h, part, &ptr, 0);
	if(rc)  return "";
	result = ptr;
	curl_free(ptr);
	curl_url_cleanup(h);
	return result;
}


string get_scheme(const string& url)
{
	return get_url_part(url, CURLUPART_SCHEME);
}

// FN url_t 
typedef struct {
	string scheme;
	string authority;
	string path;
} url_t;
// FN-END


url_t url_decompose(string url)
{


	static const std::regex re1("^([a-z]+)://([^/]+)(.*)");
	std::smatch m;
	url_t u;
	if(regex_match(url, m, re1)) {
		u.scheme = m[1];
		u.authority = m[2];
		url = m[3];
	}

	u.path = url;
	return u;
}


void print_url_decomposition(string url)
{
	cout << "Given: " << url << "\n";
	url_t u{url_decompose(url)};
	cout << "Produces: \n"
		<< "scheme: " << u.scheme
		<< "\tauth: " << u.authority
		//<< "\tport: " << u.port
		<< "\tpath: " << u.path
		<< "\n\n";
}

void test_urls()
{
	cout<< "test_urls:begin ...\n";
	print_url_decomposition("https://newsroom.st.com/media-center/press-item.html/t4536.html");
	print_url_decomposition("vscode.gmi");
	print_url_decomposition("/vscode.gmi");
	cout<< "... test_urls:\n";
	exit(0);
}

string guess_icon(const string& scheme)
{
	if(scheme == "gemini"|| scheme == "" ) return "♊";
	if(scheme == "http" || scheme == "https") return "🌐";
	if(scheme == "spartan") return "💪";
	if(scheme == "gopher") return "🦫";
	return "🧩";
}


// FN fixup_url 
void fixup_url(const string& url_in, string& url)
{
	constexpr auto debug = false;

	//cout << "url_in = " << url_in << "\n";
	auto u1{url_decompose(url)};
	if(u1.scheme != "") {
		/* seems fully formed, so don't need to do anything */
		//url = u1.schema + "://" u1.authority + u1.path;
		return;
	}

	/* url is a relative or absolute path to url_in */
	auto u2{url_decompose(url_in)};
	if(url.starts_with("/")) {
		/* It's an absolute path, so pretty straightforward */
		url = u2.scheme + "://" + u2.authority + u1.path;
		if(debug) url += " 1"; // for testing purposes
		return;
	}


	/* It's a relative path, so strip off the current document */
	auto p2{u2.path};
	string p3;
	reverse(p2.begin(), p2.end()); /* it's easier to work backwards */
	for(auto c : p2) {
		if(c == '/') break;
		p3 += c;
	}
	reverse(p3.begin(), p3.end());
	url = u2.scheme + "://" + u2.authority + "/" + p3 + url;
	if(debug) url += " 2"; // for testing purposes
}

// FN links_resolve 
// url_in tells us from where we are calling. It is needed
// to expand partial urls in the document
links_t links_resolve(phrases_t& phrases, const string& url_in)
{
	links_t links;
	int num_links = 0;
	for(auto& p : phrases) {
		if(not holds_alternative<link_t>(p)) continue;
		auto& link = std::get<link_t>(p);
		auto& url = link.url;

		string scheme = get_scheme(url);
		link.icon = guess_icon(scheme);
		fixup_url(url_in, url);
		link.num = num_links++;
		links.push_back(link);
	}
	return links;
}
