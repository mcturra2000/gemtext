#!/usr/bin/env python3

from glob import glob

from markdown import markdown
#/data/data/repos/gemtext/blog

def save(text, fname):
	dest = "/var/www/html/" + fname
	with open(dest, "w", encoding="utf-8") as f:
		f.write(text)


mds = glob("*.md")
mds.sort()
mds.reverse()

# create an index
index_md = "# Blog for 2024\n\n"
for md in mds:
	f = open(md, "r")
	title = f.readline()[:-1]
	title = title[2:]
	dstamp = f.readline()[:-1]
	link = md[:-2] + "html"
	entry = "\n\n{dstamp} [{title}]({link})".format(dstamp=dstamp, title=title, link=link)
	index_md += entry
	f.close()

index_htm = markdown(index_md)
save(index_htm, "index.html")
print(index_htm)

# now create all the htmls
for md in mds:
	with open(md, "r", encoding="utf-8") as f:
		text = f.read()
		html = markdown(text)
		fname = md[:-2] + "html"
		save(html, fname)

