(define-module (gmi)
	       #:export (gmi-test for/each fetch-url collecting collect/list $+ spit slurp)
	       #:use-module (srfi srfi-1) 
	       #:use-module (srfi srfi-26) ; cut
	       #:use-module (ice-9 rdelim) 
	       #:use-module (ice-9 textual-ports) ; get-string-all
	       #:use-module (ice-9 popen))

(define (gmi-test)
  (format #t "hello from gmi.scm\n"))

(define-syntax-rule (for/each el lst body ...)
	(let loop ((alist lst))
		(unless (null? alist)
			(let ((el (car alist)))
				body ...)
			(loop (cdr alist)))))



#! Examples:
(display (collecting col (col 1) (col 2) #t))
(display (collecting col (for/each el '(1 2) (col el))))
!#
(define-syntax-rule (collecting collector body ...)
		    (let* ((result '())
			   (collector (lambda (x) (set! result (cons x result)))))
		      (begin body ...)
		      (reverse result)))



#! Example:
(display (collect/list col el '(1 2 3 4)
		       (when (odd? el)
			 (col el)
			 (col el)
			 #t)))
!#
(define-syntax-rule (collect/list collector el lst body ...)
		    (collecting collector
				(for/each el lst body ...)))


(define $+ string-append)

(define (read-cmd cmd) (with-input-from-port (open-input-pipe cmd) read-string))

(define (fetch-url url) (read-cmd ($+ "bombadillo -p " url)))

;(define (spit file-name text) (call-with-output-file file-name (cut display text <>)))
;(define (spit file-name text) (with-output-to-file file-name (display text )))
(define (spit file-name text)
  (define p (open-file file-name "w"))
  (display text p)
  (close-port p)
  #t)

(define (slurp file-name) (call-with-input-file file-name get-string-all))

