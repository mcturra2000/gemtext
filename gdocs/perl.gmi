# PERL
Created 2022-09-08 Updated 2022-12-06

## CPAN

Dead simple to install:
```
cpan Text::Format # or whatever module you want to install
```


## Execute script

```
my $output = `script.sh --option`;
```

## given (like switch)

```
use feature qw(switch);
no warnings 'experimental'; # at least for v5.32.1

given ($scheme) {
        when("gemini")  { print "found gemini"; }
        default         { print "using default"; }
}
```

