#!/usr/bin/env sh
exec guile -s "$0" "$@"
!#

; a gopher client
(use-modules (ice-9 rdelim))
(use-modules (ice-9 popen))
(use-modules (srfi srfi-26))
(use-modules (ice-9 textual-ports))
(add-to-load-path ".")
(use-modules (gmi))

(define (rl) (load "client.scm"))

(define home "gopher://tozip.chickenkiller.com")
(define tmp-file "/tmp/gopher.txt")

(define (get-home)
  (define str (fetch-url home))
  (spit tmp-file str)
  #t)

(unless (access? tmp-file R_OK) (get-home))

(define contents (slurp tmp-file))
(define strs (string-split contents #\newline))


(for/each line strs
	  (define line1 (string-delete #\return line)) ; https://www.gnu.org/software/guile/manual/html_node/Characters.html
	  (define parts (string-split line1 #\tab))
	  (display parts)
	  (newline)
	  #t)

;(display str)
(display "\nFinished\n")


(display (collecting col (col 'hello) (col 'world) ))
