# Gemini protocol status codes
Created 2025-02-16

1 INPUT (a query sent to the server)
2 SUCCESS
3 REDIRECT
4 TEMPORARY FAILURE
5 PERMANENT FAILURE
6 CERTFICATE REQUIRED
 

## See also
=> gem.gmi	gemini protocol
