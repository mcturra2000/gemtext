# Stocks
Created 2023-03-31

## Chap 1 : Terry Smith Stockopedia screen

Roland's criteria:
* C1 : Economic sector includes consumer cyclical, consumer defensives, technology, telecoms, healthcare and industrials
* C2 : Long-term average ROCE greater than 14%
* C3 : Operating profit margin (TTM) greater than 15%
* C4 : Net debt less than five times TTM net profit
* C5 : Interest cover (TTM) greater than 10x
* C6 : Price to free cash flow (TTM) less than 30 (equivalent to 3.3% FCF yield)
* C7 : Free cash flow 5y growth rate greater than 0%
* C8 : Market capitalisation greater than £500m

=> https://app.stockopedia.com/content/building-a-terry-smith-portfolio-with-uk-shares-961653?order=createdAt&sort=desc&mode=threaded	Link

As an example, DTY (Dignity), in 2021 Net profit was £12.1m, net debt was £+556m. So C4 = 556/12.1 = 46, which is way too high. In 2016, NP= 57.2, NDebt = 524, so even at that stage, it was too high.

Contrast with CCC in 2021, which had negative net debt. So it's good. 

ULVR 2022: net debt = 22916, net profit = 7642 => C4 = 22916/7642 = 3.0. So it's OK.

BNZL 2022: net debt = 1627, net profit = 474 => C4 = 1627/474 = 3.4. Safe

DPLM 2022: net debt = 398, net profit = 94.7 => C4 = 398/94.7 = 4.2. Safe



## EXITS

=> https://lewissrobinson.com/beating-the-uk-indices-in-10-hours-a-year/ Beating the UK indices in 10 hours a year - Lewis Robinson	
=> https://qualitycompounding.substack.com/p/how-to-invest-in-quality-companies	How to invest in Quality Companies - Terry Smith
=> https://app.stockopedia.com/content/roce-deciles-of-the-ft-100-and-ft-250-indices-964779?order=createdAt&sort=desc&mode=threaded	ROCE deciles of the FT100/FT250 indices (2023-03-17) - Stockopedia
