# A TCP client in Racket scheme
Created 2022-06-05

So far I've got a Spartan server set up, and made bombodillo speak with the protocol. I noticed that there was some discussion on Gemini about having a modular browser.

It got me to thinking about how this could be best accomplished. The problem with languages like C and Go is that they're very static. If you want something that is more flexible, with pluggable architecture, then you really need a pluggable language, one that supports late binding. 

This leads one to think about languages like Scheme and Lisp. Maybe Smalltalk and Forth would fit the bill, but I'm going to say "no" to that. On the less wacky end of the scale might be Python, Lua, or even Tcl. Tcl is actually an interesting choice, I think, because it is a very command-centric language.

I guess one could achieve dynamism via Unix pipe mechanisms. Meh, it would be modular, but possibly a lot of fiddle to get the plumbing working correctly. It got me to thinking that Scheme would be the most viable solution. To that end I put together a little Racket proggy that fetched a file over my Spartan server. Here's the code:


```racket source code
#lang racket
(require racket/tcp)
(define-values (pin pout) (tcp-connect "localhost" 3000))
(display "localhost /index.gmi 0\r\n" pout)
(flush-output pout)
(tcp-abandon-port pout)

(define response (read-line pin))
(displayln response)

(define (get-lines)
  (let loop ()
    (define line (read-line pin))
    (unless (eof-object? line)
      (display line)
      (loop))))
  
(get-lines)

(tcp-abandon-port pin)

```

It does not do much, for sure. However, I am not a Schemer, so I just wanted to get /something/ working. I am not a Go programmer either, for that matter. I am more into C/C++, with proficiency in Python.

The output is as expected:
```output
2 text/gemini

# Index

Created 2022-05-19. Updated 2022-05-29


=> about.gmi            about me and this site
=> audio.gmi            audio
=> blog.gmi             blog
=> chirps.gmi           chirps
=> freebsd-firewall.gmi FreeBSD firewall
=> gemini.gmi           gemini
=> gopher.gmi           gopher
=> platformio.gmi       plaformio
=> spartan.gmi          spartan protocol
=> systemd.gmi          systemd
=> test.txt             test text file
=> unicode.gmi          unicode
=> vim.gmi              vim (digraphs)
=> vscode.gmi           visual studio code
```

I did have some frustration with getting the code working, principally because I do not know Scheme or Racket. I got there in the end, though. I might see if I can take the experiement further.

That's your lot for now, though.
