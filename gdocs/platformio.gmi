# platformio
Created 2022-05-21. Updated 2022-05-24

## platform.ini

```
[env:firebeetle32]
platform = espressif32
board = firebeetle32
framework = arduino
build_unflags = -std=c++11
build_flags = -std=c++17
```

## See also

=> vscode.gmi   vs code
