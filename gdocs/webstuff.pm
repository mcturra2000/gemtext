package webstuff;

require Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(slurp spit insertio);

sub slurp {
    my $file = shift;
    open my $fh, '<', $file or die;
    local $/ = undef;
    my $cont = <$fh>;
    close $fh;
    return $cont;
}

sub spit {
	my ($fname, $contents) = @_;
	open my $fh, '>', $fname or die;
	print $fh $contents;
	close $fh;
}

sub insertio {
	my ($line, $fname) = @_;
	my $contents = slurp($fname);
	my ($before, $after) = split(/---/, $contents, 2);
	$contents = "$before---\n$line$after";
	spit $fname, $contents;
}


1;
