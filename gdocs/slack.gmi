# SLACKWARE 15.0
Created 2023-06-15 (2017-09-16) Updated 2023-12-25

Install with Static IP 192.168.1.11/24

## LILO

vim /etc/lilo.conf
```
timeout = 0
```

Update the changes:
```
lilo
```

## WIFI

```
wpa_passphrase LUNCARTY mypassword >>/etc/wpa_supplicant.conf
```

In /etc/resolv.conf, add line before other nameserver commands (ordering is important):
```
nameserver 192.168.0.14
```

Ensure /etc/rc.d/rc.local has:

```
ifconfig eth0 down
wpa_supplicant -Dnl80211 -iwlan0 -c /etc/wpa_supplicant.conf &
ifconfig wlan0 up
ifconfig wlan0 inet 192.168.0.11
ip route add default via 192.168.0.1 dev wlan0

```

(NB that's -Dn el 180211)
See: db07.181


## USER
sudo adduser pi
sudo usermod -a -G dialout,netdev pi

## LOCALE
dotfiles/slackware/kbd
<strike>
vim /etc/profile.d/lang.sh
export LANG=en_GB.UTF-8
export LC_COLLATE=C
</strike>

## MAN
`makewhatis' - makes manpages

## RECONFIGURE
netconfig

## GROUPS

```
A	- 	The base system. Contains enough software to get up and running and have a text editor and basic communications programs.
AP	- 	Various applications that do not require the X Window System.
D	- 	Program development tools. Compilers, debuggers, interpreters, and man pages. It's all here.
E	- 	GNU Emacs. Yes, Emacs is so big it requires its own series.
F	- 	FAQs, HOWTOs, and other miscellaneous documentation.
K	- 	The source code for the Linux kernel.
KDE	- 	The K Desktop Environment. An X environment which shares a lot of look-and-feel features with the MacOS and Windows. The Qt widget library is also in this series, as KDE requires it to function.
KDEI	- 	Language support for the K Desktop Environment.
L	- 	System libraries.
N	- 	Networking programs. Daemons, mail programs, telnet, news readers, and so on.
T	- 	teTeX document formatting system.
TCL	- 	The Tool Command Language, Tk, TclX, and TkDesk.
X	- 	The base X Window System.
XAP	- 	X applications that are not part of a major desktop environment. For example Ghostscript and Netscape.
XFCE	-	Xfce window manager
Y	- 	Games (the BSD games collection, Sasteroids, Koules, and Lizards).
```

## SWAP

```
fallocate -l 3G /swapfile
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile
/etc/fstab: /swapfile none swap defaults 0 0
```


## sbopkg

### Obtaining

https://www.sbopkg.org/downloads.php

### Quickstart

Dependency resolution, and installation in a oner, example:
```
sqg -p cinelerra
sbopkg -i cinelerra # Use (Q)ueuefile rather than package
```



### Directories

/var/lib/sbopkg/SBo/15.0
	sbo pkg files



### Other

NOT RECOMMENDED (time-consuming): download ALL the queuefiles for sbopkg:
```
	sqg -a
```

## Slackbuilds


Contains binary builds for many extras, inc keepassx, sshfs, qt5:
=> /www.slackware.com/~alien/slackbuilds	Slackbuilds
