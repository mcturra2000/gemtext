package main

import (
	"bytes"
	//"errors"
	"fmt"
	//"io"
	"net"
	"os"
	"os/exec"
	"strings"
)



const (
	CONN_PORT = "7070"
	CONN_TYPE = "tcp"
)

func main() {
	hostname, _ := os.Hostname()
	fmt.Printf("Hostname: %s", hostname)
	// Listen for incoming connections.
	l, err := net.Listen(CONN_TYPE, hostname + ":" + CONN_PORT)
	if err != nil {
		fmt.Println("Error listening:", err.Error())
		os.Exit(1)
	}
	// Close the listener when the application closes.
	defer l.Close()
	fmt.Println("Listening on " + hostname + ":" + CONN_PORT)
	for {
		// Listen for an incoming connection.
		conn, err := l.Accept()
		if err != nil {
			fmt.Println("Error accepting: ", err.Error())
			os.Exit(1)
		}
		// Handle connections in a new goroutine.
		go handleRequest(conn)
	}
}

func get_path(conn net.Conn) (string, error) {

        //oops := func(msg string) (string, error) { return "", errors.New(msg) }

        buf := make([]byte, 1024) // buffer to hold incoming data
        reqLen, err := conn.Read(buf) // Read the incoming connection into the buffer.
        //fmt.Println("reqLen:", reqLen)
        if err != nil { return "", err }
        request := string(buf[:reqLen])
        request = strings.TrimRight(request, "\r\n")
	fmt.Printf("request is <%s>\n", request)
	/*
        fields := strings.Fields(request)
        if len(fields) != 3 {
                str := "Bad request. Expected 3 fields, got '" + request + "'"
                fmt.Println(str)
                return oops(str)
        }
	*/


        path := request
	/*
        if path_in == "/" { path_in = "index.gmi" }
        path, err := filepath.Abs(DATA_DIR + "/" + path_in)
        if err != nil { return "", err }

        fmt.Println("path requested:", path)

        // check for out-of-directory stuff
        if len(path) < len(DATA_DIR) { return oops("path name is too short") }
        if DATA_DIR != path[0:len(DATA_DIR)] { return oops("data root violated") }

        info, err := os.Stat(path)
        if err != nil { return "", err }
        if info.IsDir() { return oops("Won't serve directories") }

        fmt.Println("server=", fields[0])
        fmt.Println("loc=", path)
        fmt.Println("Received request ", string(buf[0:reqLen]))
	*/
        return path, err
}

func write_str(conn net.Conn, str string) {
	conn.Write([]byte(str + "\r\n"));
}

// Handles incoming requests.
func handleRequest(conn net.Conn) {
	defer conn.Close()
	//conn.SetLinger(0)

	/*
	buf := make([]byte, 1024) // buffer to hold incoming data
	reqLen, err := conn.Read(buf) // Read the incoming connection into the buffer.
	//fmt.Println("reqLen:", reqLen)
	if err != nil { return "", err }
	request := string(buf[:reqLen])
	request = strings.TrimRight(request, "\r\n")
	*/

	pathname, _ := get_path(conn)
	fmt.Println("Requested Path: ", pathname)

	write_str(conn, "hello world");
	return

	// spawn a command
	cmd := exec.Command("./nofer")
	/*
	cmdIn, _ := cmd.StdinPipe()
	cmdOut, _ := cmd.StdoutPipe()
	cmd.Start()
	cmdIn.Write([]byte(pathname + "\r\n"))
	cmdIn.Close()
	response, _ := io.ReadAll(cmdOut)
	cmd.Wait()
	*/
	cmd.Stdin = strings.NewReader(pathname + "\r\n")
	var out bytes.Buffer
	cmd.Stdout = &out
	err := cmd.Run()
	if err != nil {
		fmt.Println("Error in cmd.Ru):", err)
	}
	conn.Write([]byte(out.String()))
	fmt.Println("Exiting handler")
	//cmd.Close()
	//fmt.Print(string(cmdBytes))
	//resp1 := string(response) 
	//conn.Write([]byte(resp1))
}

