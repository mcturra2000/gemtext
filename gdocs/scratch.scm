(use-modules (srfi srfi-1)) 
(use-modules (ice-9 ftw)) ; for scandir
(use-modules (srfi srfi-26)) ; for cut
(use-modules (ice-9 rdelim)) ; for read-line
(use-modules (web uri))
(add-to-load-path ".")
(use-modules (gmi))


(display (string->uri "gopher://tozip:70/foo"))


