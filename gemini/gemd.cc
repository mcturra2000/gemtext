/*
 * 2023-06-08	Started
 * Run mkcerts to generate certificates
 * https://wiki.openssl.org/index.php/Simple_TLS_Server
 *
 * Filesystem:
 * https://en.cppreference.com/w/cpp/filesystem/canonical
 *
 * realpath:
 * https://pubs.opengroup.org/onlinepubs/9699919799/functions/realpath.html
 *
 * curl url:
 * https://curl.se/libcurl/c/libcurl-url.html
 */

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <curl/curl.h>
#include <setjmp.h>

#include <exception>
#include <filesystem>
#include <iostream>
#include <fstream>
//#include <format> // still not in g++ 12.2.0

using namespace std;
namespace fs = std::filesystem;

void puke(string msg)
{
	throw std::runtime_error(msg);
}

fs::path docdir; // path to where the gmi files are

int create_socket(int port)
{
	int s;
	struct sockaddr_in addr;
	memset(&addr, 0, sizeof(struct sockaddr_in));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = htonl(INADDR_ANY);
	//addr.sin_addr.s_addr = inet_addr("192.168.0.13");
	//addr.sin_addr.s_addr = inet_addr("127.0.0.1");

	s = socket(AF_INET, SOCK_STREAM, 0);
	if (s < 0) {
		perror("Unable to create socket");
		exit(EXIT_FAILURE);
	}

	// allow for socket reuse so we don't get "Unable to bind: Address already in use"
	int yes=1; //char yes='1'; // use this under Solaris
	if (setsockopt(s, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) == -1) {
		perror("setsockopt");
		exit(1);
	}

	if (bind(s, (struct sockaddr*)&addr, sizeof(addr)) < 0) {
		perror("Unable to bind");
		exit(EXIT_FAILURE);
	}

	if (listen(s, 1) < 0) {
		perror("Unable to listen");
		exit(EXIT_FAILURE);
	}

	return s;
}

SSL_CTX *create_context()
{
	const SSL_METHOD *method;
	SSL_CTX *ctx;

	method = TLS_server_method();

	ctx = SSL_CTX_new(method);
	if (!ctx) {
		perror("Unable to create SSL context");
		ERR_print_errors_fp(stderr);
		exit(EXIT_FAILURE);
	}

	return ctx;
}

void configure_context(SSL_CTX *ctx)
{
	/* Set the key and cert */
	if (SSL_CTX_use_certificate_file(ctx, "/home/pi/.blippy/gemini/cert.pem", SSL_FILETYPE_PEM) <= 0) {
		ERR_print_errors_fp(stderr);
		exit(EXIT_FAILURE);
	}

	if (SSL_CTX_use_PrivateKey_file(ctx, "/home/pi/.blippy/gemini/key.pem", SSL_FILETYPE_PEM) <= 0 ) {
		ERR_print_errors_fp(stderr);
		exit(EXIT_FAILURE);
	}
}

string make_reply(string& url_path)
{
	string reply;

	// reconstitute the file
	if(url_path == "/") url_path = "/index.gmi";
	cout << "docdir :'" << docdir << "', url_path:'" << url_path << endl;
	auto fname = docdir;
	fname.concat(begin(url_path), end(url_path)) ; // TODO might throw
	cout << "pre-Canonical name: '" << fname << endl;
	try {
		fname = canonical(fname);
	} catch(...) {
		return "51 Not found!\r\n";
	}

	// check request doesn't escape docdir for security reasons
	// I don't this can be activated, though, because curl does some preprocessing
	// Worth checking anyway.
	string canfile{fname}; // .u8string();
	int doclen = docdir.u8string().size();
	if(canfile.size() < doclen || docdir != canfile.substr(0, doclen))
		return "50 Permanent failure\r\n";

	cout << "Canonical name: '" << fname << endl;
	reply = "20 text/gemini\r\n";
	auto ext = fname.extension();
	if(ext == ".jpg" ) reply = "20 image/jpg\r\n";
	if(ext == ".txt" ) reply = "20 text/plain\r\n";

	std::ifstream input(fname, std::ios::in | std::ios::binary);
	std::stringstream sstr;
	sstr << input.rdbuf();
	input.close();

	reply += sstr.str();
	return reply;
}

#define PERR(err, ret) if(err == ret) puts(#ret);

void print_error(SSL *ssl, int status)
{
	int err = SSL_get_error(ssl, status);
	printf("SSL error:%d:", err);
	PERR(err, SSL_ERROR_NONE);
	PERR(err, SSL_ERROR_ZERO_RETURN);
	PERR(err, SSL_ERROR_WANT_READ);
	PERR(err, SSL_ERROR_WANT_WRITE);
	PERR(err, SSL_ERROR_WANT_CONNECT);
	PERR(err, SSL_ERROR_WANT_ACCEPT);
	PERR(err, SSL_ERROR_WANT_X509_LOOKUP);
	PERR(err, SSL_ERROR_WANT_ASYNC);
	PERR(err, SSL_ERROR_WANT_ASYNC_JOB);
	PERR(err, SSL_ERROR_WANT_CLIENT_HELLO_CB);
	PERR(err, SSL_ERROR_SYSCALL);
	PERR(err, SSL_ERROR_SSL);
	printf("print_error exiting. Should have printed something above. Try man SSL_get_error\n");

}

class Client {
	public:
		Client(int sock) {
			struct sockaddr_in addr;
			unsigned int len = sizeof(addr);
			client = accept(sock, (struct sockaddr*)&addr, &len);
			if (client < 0) {
				puke("Client ctor: Unable to accept");
			}
		};
		~Client() {
			printf("Client dtor\n");
			if(client>=0) close(client);
		};
		const int operator()() { return client; };
	private:
		int client = -1;
};

class Context {
	public:
		Context() {
			ctx = create_context();
			configure_context(ctx);
		};
		~Context() {
			printf("Context dtor called\n");
			SSL_CTX_free(ctx);
		};
		SSL_CTX* operator()() { return ctx; };
	private:
		SSL_CTX* ctx = NULL;
};

class Ssl {
	public:
		Ssl(Client& client, Context& ctx);
		~Ssl();
		void handle();
	private:
		SSL* ssl = 0;
};

Ssl::Ssl(Client& client, Context& ctx)
{
	ssl = SSL_new(ctx());
	if(ssl == NULL) {
		puke("SSL_new failed");
	}
	int status = SSL_set_fd(ssl, client());
	if(status == 0) {
		puke("SSL_set_fd failed");
	}

	if (SSL_accept(ssl) <= 0) {
		puke("SSL_accept failed");
	}
}
Ssl::~Ssl()
{
	printf("Ssl dtor called\n");
	if(SSL_get_shutdown(ssl) & SSL_SENT_SHUTDOWN) {
		printf("SSL_SENT_SHUTDOWN apparently. Skipping SSL_shutdown\n");
	} else {		
		SSL_shutdown(ssl);
	}
	SSL_free(ssl);
}

void Ssl::handle () // (Client& client, SSL_CTX *ctx)
{
	size_t nread;
	char in[100];
	int status = SSL_read_ex(ssl, in, sizeof(in)-1, &nread);
	if(status == 0) {
		// this seems to happen on Slackware, generating a SIGBUS Bus Error
		// This seems to come  from making a call like
		// gemini://tozip.chickenkiller.com/2022-06-09-fetch-tls.gmi/
		// which produces a certificate error (because tozip != inspiron)
		printf("SSL_read_ex error: ");
		print_error(ssl, status);
		return;
	}
	in[nread] = 0;
	for(int i = 0; i < strlen(in); i++){
		char c = in[i];
		if(c == '\r' || c == '\n') in[i] = 0;
	}
	printf("Read from client (chomped): '%s'\n", in);

	// decode parts of url passed in from client
	CURLU *h = curl_url();
	CURLUcode rc;
	rc = curl_url_set(h, CURLUPART_URL, in, CURLU_NON_SUPPORT_SCHEME); // unsupported in the sense of 'gemini'
	if(rc) {
		printf("curl_url_set returned error code %d\n", rc);
	}
	char* url_path;
	string upath;
	rc = curl_url_get(h, CURLUPART_PATH, &url_path, 0);
	if(rc) {
		printf("curl_url_get returned error code %d\n", rc);
		return;
	} else {
		upath = url_path; 
		cout << "upath:'" << upath << endl;
		//printf("the path is '%s'\n", url_path);
		curl_free(url_path);
	}
	curl_url_cleanup(h);

	auto reply = make_reply(upath);
	//const char *rstr = reply.c_str();
	//SSL_write(ssl, rstr, strlen(rstr));
	SSL_write(ssl, reply.c_str(), reply.size());
}

class Socket {
	public:
		Socket() {
			sock = create_socket(1965);
		};
		~Socket() {
			printf("Socket: closing\n");
			close(sock);
		};
		int& operator()() { return sock; };
	private:
		int sock;

};

jmp_buf ctl_c;

//bool stop =false;
void int_handler(int dummy) // Ctl-C
{
	printf("int_handler called\n");
	longjmp(ctl_c, 1);
}


int main(int argc, char **argv)
{
	// work out where the docs dir is
	auto fs1 = fs::path(argv[0]);
	auto fs2 = canonical(fs1);
	auto fs3 = fs2.remove_filename(); // /abs/path/to/exe/ (with trailing /)
	fs3 += "../gdocs";
	docdir = canonical(fs3);
	cout << "docdir:" << docdir << endl;

	int status;

	signal(SIGPIPE, SIG_IGN); /* Ignore broken pipe signals */
	signal(SIGINT, int_handler); // handle Ctl-C


	Context ctx;
	Socket socket;
	if(setjmp(ctl_c) == 0) {
		/* Handle connections */
		while(1) {
			//std::exception_ptr eptr;
			try {
				Client client(socket());
				Ssl ssl(client, ctx);
				ssl.handle();
				//do_client(client, ctx());
			} catch (const std::exception& ex) {
				//eptr = std::current_exception();
				cout << "Exception caught: " << ex.what() << endl;
			}
		}
	}
	printf("main exiting\n");
}
