#!/bin/sh

NAME=`basename -s .service *.service`
WHO=pi

echo "service is $NAME"

print_help()
{
cat <<EOF
configuration script for systemd
With no arguments, it starts the service
-h	this help
-i	install systemd stuff
-j	journal status
-a ARG	some arg
-r	start or restart
-t	tail log
-x	stop
EOF
}


do_install()
{
	sudo loginctl enable-linger $WHO
	sudo cp $NAME.service /etc/systemd/system
	sudo systemctl daemon-reload
	sudo systemctl start $NAME
	sudo systemctl enable $NAME
	exit 0
}

do_journal()
{
	sudo systemctl status $NAME | cat
	journalctl -u $NAME | cat
	exit 0
}

do_restart()
{
	sudo systemctl restart $NAME
	exit 0
}

do_stop()
{
	sudo systemctl stop $NAME
	exit 0
}

while getopts "a:hijrtx" opt; do
case ${opt} in
a) echo "arg is: $OPTARG" ;;
h) print_help ; exit ;;
i) do_install ;;
j) do_journal ;;
r) do_restart ;;
t) tail -f ~/.gemini/$NAME.log ;;
x) do_stop ;;
?) echo "Unrcognised argument" ; exit ;;
esac
done

#/data/data/repos/gemtext/gemini/gemd >>/home/pi/.gemini/gemd.log
#sudo systemctl start $NAME

