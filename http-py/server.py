# https://docs.python.org/3/library/http.server.html

import html
import re

link_re = re.compile(r"^=>\s+(\S+)\s+(.*)")
hdr_re = re.compile(r"^(#{1,3})\s*(.+)")

# from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer # python2
from http.server import BaseHTTPRequestHandler, HTTPServer # python3
class HandleRequests(BaseHTTPRequestHandler):
	def _set_headers(self):
		self.send_response(200)
		self.send_header('Content-type', 'text/html')
		self.send_header('charset', 'UTF-8') # doesn't seem to help. See calt.gmi for example problem
		self.end_headers()

	def sendstr(self, s) :
		self.wfile.write(bytes(s, "utf-8"))

	def sendln(self, s):
		self.sendstr(s)
		self.sendstr('<br>\n')

	def sendlink(self, link, desc):
		self.sendln('<a href="' + link + '">' + desc + '</a>')

	def process_line(self, line):
		#line = str(line)
		#print(line)
		if line.startswith("```"):
			self.pre = not self.pre
			if self.pre:
				self.sendln('<pre>')
			else:
				self.sendln('</pre>')
			return

		if self.pre:
			line = html.escape(line)
			self.sendstr(line)
			return

		m = link_re.match(line)
		if m :
			self.sendlink(m.group(1), m.group(2))
			return
			#link = m.group(1)
			#text = m.group(2)
			#line = '<a href="' + link + '">' + text + '</a>'

		m = hdr_re.match(line)
		if m:
			depth = len(m.group(1))
			depth = str(depth)
			text = m.group(2)
			line = "<h" + depth + '>' + text + '</h' + depth + '>\n'
			self.sendstr(line)
			return

		self.sendln(line)
			#self.sendstr(line)

	def do_GET(self):
		self._set_headers()
		p = self.path
		if p == '/': p = '/index.gmi'
		p = "../gdocs" + p # TODO make secure
		with open(p, encoding='utf-8') as f:
			contents = f.readlines()
		print(self.path)
		#self.wfile.write(bytes("received get request", "utf-8"))
		#self.wfile.write(bytes(contents, "utf-8"))
		self.sendlink("/", "Home")
		self.pre = False # preformatted
		for line in contents:
			self.process_line(line)
		
		
	def do_POST_XXX(self):
		'''Reads post request body'''
		self._set_headers()
		content_len = int(self.headers.getheader('content-length', 0))
		post_body = self.rfile.read(content_len)
		self.wfile.write("received post request:<br>{}".format(post_body))

	def do_PUT_XXX(self):
		self.do_POST()

host = ''
port = 8000
print("Port:", port)
HTTPServer((host, port), HandleRequests).serve_forever()

