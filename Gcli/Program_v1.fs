﻿module Ver1

//open MyInterop
open Option
open System
//open System.Diagnostics
open System.IO
open FParsec


type Line =
    | Link of string * string // url, description
    | Heading of string
    | Normal of string

let most = manySatisfy (function ' '|'\t' -> false | _ -> true)

let mkLink (u:string, d:string) = Link(u, d)
let link = skipString "=>" >>. spaces  >>. most .>> spaces .>>. restOfLine false  |>> mkLink
let heading = pchar '#' .>>. restOfLine false |>> fun (x, y) -> Heading( "#" + y)
//let heading = pchar '#' <?> restOfLine false |>> fun (x, y) -> Heading( "#" + y)
let normalLine =    restOfLine false  |>> Normal // >>% Line.Normal


let line =  choice [link; heading ; normalLine]


let str = """# This is a heading
## And so is this
This is just a normal line
=> linkypoo.gmi And this is a link for you
"""

let consoleColor (fc : ConsoleColor) = 
    let current = Console.ForegroundColor
    Console.ForegroundColor <- fc
    { new IDisposable with
          member x.Dispose() = Console.ForegroundColor <- current }

// printf statements that allow user to specify output color
let cprintf color str = Printf.kprintf (fun s -> use c = consoleColor color in printf "%s" s) str
let cprintfn color str = Printf.kprintf (fun s -> use c = consoleColor color in printfn "%s" s) str


let printLine (lin) =
    match lin with
    | Link (u, d) -> printfn "%s\n ... %s" d u
    | Heading (s) -> cprintfn ConsoleColor.Cyan"%s"  s
    | Normal (s) -> printfn "%s" s


let getLine lin =
    //printfn "%A" lin
    let presult = run line lin
    match presult with
    | Success (lin1, _, _) -> lin1
    | Failure (err, _, _) -> Normal("Impossible err") // can't really get here though



let main1 () =
    let lines = str.Split[|'\n'|]
    for lin in lines do
        lin |> getLine |> printLine

(* ************************************************************************************************ *)

let printLines (lines) =
    for lin in lines do
        lin |> getLine |> printLine
let main2 (args : string array) = 
    let file = if args.Length = 0 then "index.gmi" else args[0] 
    let filePath = "/home/pi/repos/gemtext/gdocs/" + file
    let readLines  = System.IO.File.ReadLines(filePath)
    printLines readLines
    printfn "\x1B[31;1;4mThis should be bright red\x1B[0m" // NB F# doesn't understand octal
    0

(* ************************************************************************************************ *)

let pathFromCmdLine (args : string array) = 
    let file = if args.Length = 0 then "index.gmi" else args[0] 
    "/home/pi/repos/gemtext/gdocs/" + file

let slurpLines (filePath) = 
    filePath |> System.IO.File.ReadLines


let spaceLike = [ '\t'; ' '; '\a']   // should probably include some other unicode stuff

let aspace = anyOf spaceLike |>> string
let nonSpaces = many1Satisfy   (fun c -> List.contains c spaceLike |> not)  |>> string
let wordifyParser = many (aspace <|> nonSpaces) 
let wordify (lin) = 
    let p = run wordifyParser lin
    match p with 
    | Success (lin1, _, _ ) ->lin1
    | Failure (err, _, _) -> [""]

let printStr (s) = printf "%s" s
let main3 (args : string array) =
    //let printEvery (x) = every printStr x
    for lin in pathFromCmdLine args |> slurpLines do
        let words = wordify lin
        for w in words do  printStr w
        printStr "\n"
    0

(* ************************************************************************************************ *)

let wordLen p word = if word = "\t" then 8-(p % 8) else word.Length 


// these don't account for leading and trailing spaces
let mkCrudeSlugs width wordList = 
    let mutable pos = 0
    let mutable built: string list list = [[]]
    let mutable building: string list = []
    //let accum word = 
    //let words1 = List.filter (fun x -> not (x = "\n" )) wordList
    for word in wordList do
        //printfn "BUILDT %A XXx" built
        let len = wordLen pos word
        if len + pos > width then 
            if built.Length > 0 then  
                built <- List.append built  [building]
            else
                built <- [building]
            building <- [word]
            pos <- len
        else
            pos <- pos + len
            if building.Length > 0 then 
                building <- List.append building [word]
            else
                building <- [word]
    if building.Length > 0 then built <- List.append built  [building]

    if built.Length > 1 then
        built[1..]
    else
        built

 

let printStrList (strs) = 
    for s in strs do
        printStr s

// I don't think this is actually necessary
let slurpChompedLinesXXX (filePath) = 
    //let lines = filePath |> System.IO.File.ReadLine
    let chomper s : string = String.filter (fun x -> x <> '\n') s
    let readLines (filePath:string) = seq {
        use sr = new StreamReader (filePath)
        while not sr.EndOfStream do
            let line = sr.ReadLine ()
            let chomped = chomper line
            yield chomped
    }
    readLines filePath

let main4 args =
    //let printEvery (x) = every printStr x
    let path = pathFromCmdLine args
    let lines = slurpLines path
    //printfn "%A" lines
    
    for lin in lines do
        let words = wordify lin
        //printfn "WORDS %A XXX" words
        let width = 70
        for slug in mkCrudeSlugs width words do
            //printf "%A" slug
            //for s in slug do printf "%s" s
            printStrList slug  ; printf "\n"
            ()

(* ************************************************************************************************ *)

let slurpArgLines args = args |> pathFromCmdLine |> slurpLines



type ProcessResult = { 
    ExitCode : int; 
    StdOut : string; 
    StdErr : string 
}

let executeProcess (processName: string) (processArgs: string) =
    let psi = new Diagnostics.ProcessStartInfo(processName, processArgs) 
    psi.UseShellExecute <- false
    psi.RedirectStandardOutput <- true
    psi.RedirectStandardError <- true
    psi.CreateNoWindow <- true        
    let proc = Diagnostics.Process.Start(psi) 
    let output = new Text.StringBuilder()
    let error = new Text.StringBuilder()
    proc.OutputDataReceived.Add(fun args -> output.Append(args.Data) |> ignore)
    proc.ErrorDataReceived.Add(fun args -> error.Append(args.Data) |> ignore)
    proc.BeginErrorReadLine()
    proc.BeginOutputReadLine()
    proc.WaitForExit()
    { ExitCode = proc.ExitCode; StdOut = output.ToString(); StdErr = error.ToString() }

let termCols  () = 
    let result = executeProcess "tput" "cols"
    int(result.StdOut)

let termRows  () = 
    let result = executeProcess "tput" "rows"
    int(result.StdOut)    


let mkCrudeSlugs_v2 (width : int) (wordList : list<string>)  = 
    let mutable pos = 0
    let mutable idx = 0
    let len : int = wordList.Length


    let mutable brk = false // break control flow
    let mutable built : string option list = []
    let app x = built <- x :: built    
    while idx < len do
        brk <- false
        while pos = 0 && idx < len && wordList[idx] = " "  do idx <- idx + 1 // skip leading spaces in line
        while idx < len && not brk do
            let word  = wordList[idx]
            let len1 = wordLen pos word
            if len1 > width then // it's a very long line
                for idx1 in [0.. word.Length-1] do
                    let emit () = word[idx1] |> string |> Some |> app
                    if pos < width then 
                        emit()
                        pos <- pos + 1
                    else
                        app None
                        emit()
                        pos <- 1
                    
                    
            else if len1 + pos > width then 
                app None
                //printfn "Appending None"
                pos <- 0
                idx <- idx - 1
                //app (Some word)
                brk <- true
            else
                pos <- pos + len1
                app (Some word)

            idx <- idx + 1
    built |> List.rev

      
let nl () = printfn ""


let main5 args = 
    let lines = slurpArgLines args
    let width = termCols()  
    //printfn "Width %A"   width
    for lin in lines do
        let wordList = wordify lin
        for slug in (mkCrudeSlugs_v2 width wordList) do
            if isNone slug then
                printfn ""
            else
                printStr (get slug)
        nl ()
    ()